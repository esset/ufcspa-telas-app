import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { DatePipe } from "@angular/common";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ActivitySearchPage } from '../pages/activity-search/activity-search';
import { SupportPage } from '../pages/support/support';
import { ActivityProvider } from '../providers/activity/activity';

import { IonItemCollapse, IonListCollapse } from "../components/ion-list-collapse";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ActivitySearchPage,
    SupportPage,

    IonItemCollapse,
    IonListCollapse
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    BrowserAnimationsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ActivitySearchPage,
    SupportPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ActivityProvider,
    DatePipe
  ]
})
export class AppModule {}
