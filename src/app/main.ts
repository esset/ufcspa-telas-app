import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app.module';

platformBrowserDynamic().bootstrapModule(AppModule);

if ('serviceWorker' in navigator) {
  console.log('Registering the ServiceWorker');
  navigator.serviceWorker.register('service-worker.js');
}

