import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

export const collapseAnimation = trigger('collapseAnimation', [
  state('in', style({ height: '*', opacity: '1' })),
  transition('void => *', [
    style({ height: '0', opacity: '0' }),
    animate(250)
  ]),
  transition('* => void', [
    animate(250, style({ height: '0', opacity: '0' }))
  ])
]);
