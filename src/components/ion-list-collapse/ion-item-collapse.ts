import { Component, Input, Output, EventEmitter } from '@angular/core';
import { collapseAnimation } from './collapse.animation';

@Component({
  selector: 'ion-item-collapse',
  animations: [collapseAnimation],
  styles: [
    '.item-content { text-align: justify; margin-left: 30px; }'
  ],
  template: `
    <ion-item (click)="toggleOpen()"
              [ngClass]="{ active: isOpen }">
      <ion-icon item-end name="{{ isOpen ? 'arrow-dropup' : 'arrow-dropdown' }}"></ion-icon>
      {{ this.title }}
    </ion-item>
    <div class="item-content"
         *ngIf="isOpen" [@collapseAnimation]>
      <p>
        <ng-content></ng-content>
      </p>
    </div>
  `
})
export class IonItemCollapse {

  @Input() title: string;
  @Output() onOpen: EventEmitter<any> = new EventEmitter();

  isOpen: boolean = false;

  constructor() {}

  toggleOpen() {
    this.isOpen = !this.isOpen;
    if (this.isOpen) this.onOpen.emit()
  }

  close() {
    this.isOpen = false;
  }

}
