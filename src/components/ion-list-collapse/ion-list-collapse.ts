import {Component, QueryList, ContentChildren} from '@angular/core';
import {IonItemCollapse} from "./ion-item-collapse";

@Component({
  selector: 'ion-list-collapse',
  template: `
    <ion-list>
      <ng-content></ng-content>
    </ion-list>
  `,
})
export class IonListCollapse {

  @ContentChildren(IonItemCollapse) collapses: QueryList<IonItemCollapse>;
  activeItem: IonItemCollapse = null;

  constructor() {}

  ngAfterViewInit() {
    this.collapses.forEach(item => {
      item.onOpen.subscribe(() => {
        if (this.activeItem && this.activeItem !== item)
          this.activeItem.close();
        this.activeItem = item;
      })
    });
  }

}
