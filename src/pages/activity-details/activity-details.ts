import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ActivityDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-activity-details',
  templateUrl: 'activity-details.html',
})
export class ActivityDetailsPage {

  activity: any;
  buildingImg: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.activity = this.navParams.get('activity');
    this.setBuilding();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivityDetailsPage');
  }

  setBuilding(){
    switch(this.activity.place.building) {
      case "Prédio 1": {
        this.buildingImg = "assets/imgs/p1.jpg";
        break;
      }
      case "Prédio 2": {
        this.buildingImg = "https://revistamdc.files.wordpress.com/2012/11/leste4.jpg";
        break;
      }
      case "Prédio 3": {
        this.buildingImg = "assets/imgs/p3.jpg";
        break;
      }
      case "Sec. Ensino Sta. Clara": {
        this.buildingImg = "https://www.ufcspa.edu.br/images/phocagallery/infraestrutura/thumbs/phoca_thumb_l_vista%20area%20campus%20a.jpg";
        break;
      }
      case "Guarita 1 - Norte": {
        this.buildingImg = "https://www.ufcspa.edu.br/images/phocagallery/infraestrutura/thumbs/phoca_thumb_l_vista%20area%20campus%20a.jpg";
        break;
      }
      case "Guarita 2 - Sul": {
        this.buildingImg = "https://www.ufcspa.edu.br/images/phocagallery/infraestrutura/thumbs/phoca_thumb_l_vista%20area%20campus%20a.jpg";
        break;
      }
      default: {
        this.buildingImg = "https://www.ufcspa.edu.br/images/phocagallery/infraestrutura/thumbs/phoca_thumb_l_vista%20area%20campus%20a.jpg";
        break;
      }
    }
  }

}
