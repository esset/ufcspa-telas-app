import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActivitySearchPage } from './activity-search';

@NgModule({
  declarations: [
    ActivitySearchPage,
  ],
  imports: [
    IonicPageModule.forChild(ActivitySearchPage),
  ],
})
export class ActivitySearchPageModule {}
