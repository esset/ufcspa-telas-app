import { Component, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { IonicPage, NavController, Searchbar } from 'ionic-angular';
import { ActivityProvider } from '../../providers/activity/activity';
import { SupportPage } from "../support/support";
import Utils from '../../../utils';
import "rxjs/add/operator/debounceTime";

/**
 * Generated class for the ActivitySearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-activity-search',
  templateUrl: 'activity-search.html',
})
export class ActivitySearchPage {

  @ViewChild('searchbar') searchbar: Searchbar;

  supportPage = SupportPage;

  searchTerm: string = '';
  searchControl: FormControl;
  activitiesByBuilding: any[] = [];
  activitiesByBuildingRaw: any;
  hasError: boolean = false;
  hasNetwork: boolean = true;
  isSearching: any = false;
  searchStatus: string = "moment";

  constructor(public navCtrl: NavController, public activityProvider: ActivityProvider) {
    this.searchControl = new FormControl();
    this.setupListeners();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivitySearchPage');

    this.getActivities();

    this.searchbar.setFocus();

    this.searchControl.valueChanges
      .debounceTime(700)
      .subscribe(search => {
        this.isSearching = false;
        this.searchTerm = search;
        if(this.searchTerm == '')
          this.searchStatus = "moment";
        this.getActivities();
        //this.filterItems(search);
      });
  }

  searchFullDay(){
    this.searchStatus = "all";
    this.getActivities();
  }

  getActivities() {
    console.log('getActivities()');
    this.isSearching = true;

    this.activityProvider.getActivities({ status: this.searchStatus, search: this.searchTerm })
      .subscribe(
        data => {
          this.activitiesByBuilding = data.buildings;
          //this.filterItems(this.searchTerm);
          this.isSearching = false;
        },
        error => {
          console.log(error);
          this.hasError = true;
          this.isSearching = false;
        }
      );
  }

  setupListeners() {
    this.hasNetwork = navigator.onLine;

    const updateNetworkState = () => {
      this.hasNetwork = navigator.onLine;
    };

    window.addEventListener('offline', updateNetworkState);
    window.addEventListener('online', updateNetworkState);
  }

  filterItems(target: string) {
    console.log(`filterItems(${target})`);

    if (this.activitiesByBuildingRaw) {
      this.activitiesByBuilding = [];
      let j = 0;

      for (let i = 0; i < this.activitiesByBuildingRaw.buildings.length; i++) {
        let activities = this.activitiesByBuildingRaw.buildings[i].activities.filter((activity) => {

          let matchName = Utils.normalize(activity.name)
            .toLowerCase().indexOf(Utils.normalize(this.searchTerm).toLowerCase()) > -1;

          let matchCourseName = Utils.normalize(activity.course.name)
            .toLowerCase().indexOf(Utils.normalize(this.searchTerm).toLowerCase()) > -1;

          return (matchName || matchCourseName);
        });
        if (activities.length != 0) {
          this.activitiesByBuilding[j++] = {
            name: this.activitiesByBuildingRaw.buildings[i].name,
            activities: activities
          };
        }
      }
    }
  }

  onSearchInput() {
    this.isSearching = true;
  }

  openDetails(activity) {
    this.navCtrl.push('ActivityDetailsPage', { activity });
  }

  doRefresh(refresher) {
    this.getActivities();
    //this.filterItems(this.searchbar.getNativeElement().value);
    refresher.complete();
  }

  hasActivities(){
    let flag = false;

    for(let building of this.activitiesByBuilding){
      if (building.activities.length != 0) flag = true;
    }

    return flag;
  }

}
