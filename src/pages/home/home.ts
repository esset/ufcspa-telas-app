import { Component } from '@angular/core';
import { FabContainer, NavController } from 'ionic-angular';

import { ActivitySearchPage } from "../activity-search/activity-search";
import { SupportPage } from "../support/support";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  activitySearchPage = ActivitySearchPage;
  supportPage = SupportPage;

  constructor(public navCtrl: NavController) {

  }

  closeFab(fab: FabContainer) {
    setTimeout(() => fab.close(), 250);
  }

}
