import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";

/*
  Generated class for the ActivityProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ActivityProvider {

  constructor(public http: HttpClient) {
    console.log('Hello ActivityProvider Provider');
  }

  getActivities(options: any): Observable<any> {
    let httpParams = new HttpParams();
    Object.keys(options).forEach(function (key) {
      httpParams = httpParams.append(key, options[key]);
    });
    return this.http.get('https://ufcspa-dev.herokuapp.com/v2/activities', { params: httpParams });
  }

}
