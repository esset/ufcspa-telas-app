export default class Utils {
  static normalize(val: string) {
    //Remove accents/diacritics in a string and trim
    return val.normalize('NFD').replace(/[\u0300-\u036f]/g, "").trim();
  }
}
